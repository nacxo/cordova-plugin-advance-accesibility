var exec = require('cordova/exec');

var PLUGIN_NAME = 'AdvanceAccesibility';

var AdvanceAccesibility = {
  echo: function(phrase, cb) {
	exec(cb, null, PLUGIN_NAME, 'echo', [phrase]);
  }
};

module.exports = AdvanceAccesibility;